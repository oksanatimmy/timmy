﻿#include "pch.h"
#include<iostream>
#include<string>

using namespace std;

struct production
{
	char industry[20];
	char unit[20];
	double year1;
	double year2;
	double year3;
	double year4;
};

int main()
{
	setlocale(LC_ALL, "rus");




	production iron[7] =
	{
		{"Чугун", "млн.т", 2.9, 2.4, 9.6, 22.3 },
		{ "Сталь", "млн.т", 2.4, 2.4, 8.9, 24 },
		{ "Прокат", "млн.т", 2.1, 2, 6.5, 19.6 },
		{ "Железо", "млн.т", 6.9, 4.7, 20.2, 53.5 },
		{ "Кокс", "млн.т", 4.4, 4, 15.7, 29.2 },
		{ "Уголь", "млн.т", 22.8, 24.8, 83.8, 167.7 },
		{ "Газ", "млрд.м^3", 0,0, 0.5, 11.6 }
	};

	cout << "№\t" << "Продукция\t" << "Ед.Измерения\t" << "1913\t" << "1928\t" << "1940\t" << "1959\t" << endl;
	cout << "==========================================================================================" << endl;




	cout << " 1\t " << iron[0].industry << "\t" << iron[0].unit << '\t'
		<< iron[0].year1 << '\t' << iron[0].year2 << '\t' << iron[0].year3 << '\t'
		<< iron[0].year4 << endl;
	cout << " 2\t " << iron[1].industry << '\t' << iron[1].unit << '\t'
		<< iron[1].year1 << '\t' << iron[1].year2 << '\t' << iron[1].year3 << '\t'
		<< iron[1].year4 << endl;
	cout << " 3\t " << iron[2].industry << '\t' << iron[2].unit << '\t'
		<< iron[2].year1 << '\t' << iron[2].year2 << '\t' << iron[2].year3 << '\t'
		<< iron[2].year4 << endl;
	cout << " 4\t " << iron[3].industry << '\t' << iron[3].unit << '\t'
		<< iron[3].year1 << '\t' << iron[3].year2 << '\t' << iron[3].year3 << '\t'
		<< iron[3].year4 << endl;
	cout << " 5\t " << iron[4].industry << '\t' << iron[4].unit << '\t'
		<< iron[4].year1 << '\t' << iron[4].year2 << '\t' << iron[4].year3 << '\t'
		<< iron[4].year4 << endl;
	cout << " 6\t " << iron[5].industry << '\t' << iron[5].unit << '\t'
		<< iron[5].year1 << '\t' << iron[5].year2 << '\t' << iron[5].year3 << '\t'
		<< iron[5].year4 << endl;
	cout << " 7\t " << iron[6].industry << '\t' << iron[6].unit << '\t'
		<< iron[6].year1 << '\t' << iron[6].year2 << '\t' << iron[6].year3 << '\t'
		<< iron[6].year4 << endl;

	int max = iron[0].year4;
	string maxIndustry;
	for (int i = 0; i < 7; i++)
	{
		if (iron[i].year4 > max)
		{
			max = iron[i].year4;
			maxIndustry = iron[i].industry;
		}
	}

	cout << "Максимальное значение продукции в 1928 году было: " << maxIndustry << " .\n";


	system("pause");
	return 0;
}
